﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel.Attributes;

namespace CargaDeDatos
{
    class Outcome
    {
        [ExcelColumn("CÓDIGO OUTCOME")]
        public string Codigo { get; set; }

        [ExcelColumn("NOMBRE OUTCOME")]
        public string Nombre { get; set; }

        [ExcelColumn("DESCRIPCIÓN")]
        public string Descripcion { get; set; }

        [ExcelColumn("DESCRIPCIÓN INGLÉS")]
        public string DescripcionIngles { get; set; }

        [ExcelColumn("COMISIÓN")]
        public string Comision { get; set; }
    }
}
